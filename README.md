# Advanced Redux - React App (Platform Electives Objective)
A simple React App that allows users to add items and remove to their cart. Items in the cart as well as the quantity per item and total cost of these items are then saved in to the backend (Firebase). The objective tackled in this task was the use or how to handle async in the realm of Redux.

## Setup

```
npm install
npm run start
```
